# Vax Populi #



### What is Vax Populi? ###

**Vax Populi** is a Dapp that takes secure clinical data bound to an individual identity and issues verifiable health credentials including vaccination status against **COVID-19**. 